import { Component, OnInit } from "@angular/core";
import * as lodash from "lodash";

@Component({
    selector: "ns-items",
    templateUrl: "./items.component.html",
    styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {
    minValue = 0;
    maxValue = 10;

    count = 1;
    items: number[] = [];

    constructor() { }

    ngOnInit() {}

    async generation() {
        this.changeValues();
        this.items = [];
        let arr = [];
        let numbers = lodash.range([this.minValue], this.maxValue + 1, [1]);
        let len = numbers.length;
        while(len--) {
            let random = Math.floor(Math.random() * (len + 1));
            if(this.count === arr.length) {
                break
            }
            arr.push(numbers[random]);
            numbers.splice(random, 1);
        }
        this.items = arr;
    }

    changeValues() {
        if(this.maxValue == 0) {
            this.maxValue = 1;
        }
        if(this.maxValue <= this.minValue) {
            this.minValue = this.maxValue - 1
        }
    }

}
